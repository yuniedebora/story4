from django.shortcuts import render

def index(request):
    return render(request, 'Home.html')

def profile(request):
    return render(request, 'Profile.html')

def gallery(request):
    return render(request, 'Gallery.html')